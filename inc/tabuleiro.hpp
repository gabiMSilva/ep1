#ifndef TABULEIRO_HPP
#define TABULEIRO_HPP

#include "forma.hpp"

class Tabuleiro:public Forma{
  public:
    Tabuleiro();
    ~Tabuleiro();
    void viver(int posicao_i, int posicao_j);
    void morrer(int posicao_i, int posicao_j);
    void criarForma();
    void importarMatriz(char celula, int posicao_i, int posicao_j);
    void imprimirMatriz();
    int numVizinhosVivos(int posicao_i, int posicao_j);
    void aplicarRegras();
 };
#endif
