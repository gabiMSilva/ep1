#ifndef BLINKER_HPP
#define BLINKER_HPP

#include "forma.hpp"
#include <string>

class Blinker:public Forma{
  public:
    Blinker();
    ~Blinker();
    void criarForma();
};
#endif
