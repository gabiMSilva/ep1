#ifndef INICIO_HPP
#define INICIO_HPP

class Inicio{
  private:
    int resposta;
    int geracoes;
    char celula_viva;
    char celula_morta;
  public:
    Inicio();
    ~Inicio();
    void setGeracoes(int geracoes);
    int getGeracoes();
    void setResposta(int resposta);
    int getResposta();
    char getCelulaViva();
    void setCelulaViva(char celula_viva);
    char getCelulaMorta();
    void setCelulaMorta(char celula_morta);

    void menu();
    void entrarconfiguraruracoes();
    void configurarCharCelulas();
    void inserirGeracoes();
};
#endif
