#ifndef FORMA_HPP
#define FORMA_HPP

#include <iostream>

using namespace std;

class Forma {
  private:
    char tabuleiro[32][42];
    char celula;
    char celula_morta;
    char celula_viva;

  public:

    Forma();
    ~Forma();
    char getCelulaViva();
    void setCelulaViva(char celula_viva);
    char getCelulaMorta();
    void setCelulaMorta(char celula_morta);
    void tamanhoMatriz();
    void setCelula(char celula, int posicao_i, int posicao_j);
    char getCelula(int posicao_i, int posicao_j);
    virtual void criarForma() = 0;
};
#endif
