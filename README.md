                                                         JOGO DA VIDA

Antes de começar o jogo, algumas observações são necessárias:

1. O jogo deverá ser executado com o terminal em "tela cheia (maximizado)";
2. Para executar o jogo abra o terminal no diretório do mesmo, digite "make" e, logo depois, "make run".

Instruções sobre como jogar:

1. Na primeira parte do programa aperecerá um menu no qual você terá três opções de escolha: "Entrar", "Fazer Ajustes" ou "Sair".

2. 
	2.1. "ENTRAR": A primeira opção é exclusivamente para quem deseja rodar o jogo sem fazer nenhuma alteração estética. Para acessá-la basta digitar "1" e apertar "enter" em seu teclado;

	2.2. "FAZER AJUSTES": Caso deseje fazer algumas alterações vizuais em seu jogo, escolha a opção "Fazer Ajustes" digitando "2" e, logo após, "Enter";
		
		2.2.1. Assim que você apertar "Enter" aprecerá uma tela perguntando o número de gerações que você deseja que o seu jogo tenha, ou seja, o número de "loops" que deseja que o seu programa faça. Insira um número INTEIRO  e POSITIVO e aperte "Enter";
		
		Obs.: Para uma boa vizualização do padrão, é recomendado que o jogo tenha, pelo menos, 50 gerações;
		
		2.2.2. Logo aparecerá sua próxima opção de ajuste no jogo: o caracter de células vivas. Nessa etapa você deve escolher um símbolo de sua preferência que irá representar as células que estão vivas em seu jogo. Para escolher digite um símbolo presente no seu teclado e aperte "Enter";
		Obs.: Não digite mais de caracter para representar as células vvivas ou mortas;	
		
		2.2.3. A próxima opção de ajuste é semelhante à anterior, porém será para escolher o caracter das células mortas;
		
		obs.: Por questões de estética e de melhor vizualização é recomendado escolher símbolos de tamanhos vizualmente distintos, como "O" e "." para célula viva e morta respectivamente, ou "#" e ",". Uma escolha como "$" e "#" pode deixar o jogo vizualmente confuso;
		
		2.2.4. O jogo comecerá no instante que você apertar "Enter" e parará assim que o número de gerações inseridas for alcançado;
	
	2.3. "SAIR": Caso deseje sair do jogo digite "0" e aperte "Enter". Será necessário uma CONFIRMAÇÃO se é esse mesmo o seu desejo. Se for, digite "s" e aperte "enter".

Bom jogo!





