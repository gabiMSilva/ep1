#include "tabuleiro.hpp"

Tabuleiro::Tabuleiro(){
}
Tabuleiro::~Tabuleiro(){
}
void Tabuleiro::viver(int posicao_i, int posicao_j){
  setCelula(getCelulaViva(), posicao_i, posicao_j);
}
void Tabuleiro::morrer(int posicao_i, int posicao_j){
  setCelula(getCelulaMorta(), posicao_i, posicao_j);
}
void Tabuleiro::criarForma(){
  int posicao_i, posicao_j;
  for (posicao_i=0; posicao_i<32; posicao_i++){
    for (posicao_j=0; posicao_j<42; posicao_j++){
      if (posicao_i==0 && posicao_j==24){
        setCelula(getCelulaViva(), posicao_i, posicao_j);
      }
      else if(posicao_i==1 && (posicao_j==22 || posicao_j==24)){
        setCelula(getCelulaViva(), posicao_i, posicao_j);
      }
      else if(posicao_i==2 && (posicao_j==12 || posicao_j==13)){
        setCelula(getCelulaViva(), posicao_i, posicao_j);
      }
      else if(posicao_i==3 && (posicao_j==11 || posicao_j==15)){
        setCelula(getCelulaViva(), posicao_i, posicao_j);
      }
      else if (posicao_i==5 && (posicao_j==14 || posicao_j==17 || posicao_j==22 || posicao_j==24)){
        setCelula(getCelulaViva(), posicao_i, posicao_j);
      }
      else if (posicao_i==6 && posicao_j==24){
        setCelula(getCelulaViva(), posicao_i, posicao_j);
      }
      else if (posicao_i==7 && (posicao_j==11 || posicao_j==15)){
        setCelula(getCelulaViva(), posicao_i, posicao_j);
      }
      else if (posicao_i==8 && (posicao_j==12 || posicao_j==13)){
        setCelula(getCelulaViva(), posicao_i, posicao_j);
      }
      else {
        setCelula(getCelulaMorta(), posicao_i, posicao_j);
      }
    }
  }
}
void Tabuleiro::importarMatriz(char celula, int posicao_i, int posicao_j){
  if (celula != getCelulaViva()){
    setCelula(getCelulaMorta(), posicao_i, posicao_j);
  }
  else{
    setCelula(celula, posicao_i, posicao_j);
  }
}
void Tabuleiro::imprimirMatriz(){
  int posicao_i, posicao_j;
  for (posicao_i=0; posicao_i<24; posicao_i++){
    for (posicao_j=0; posicao_j<36; posicao_j++){
        if (posicao_j < 35){
          cout << ' ' << getCelula(posicao_i, posicao_j);
        }
        if (posicao_j==35){
          cout << ' ' << getCelula(posicao_i, posicao_j) << '\n';
      }
    }
  }
}

int Tabuleiro::numVizinhosVivos(int posicao_i, int posicao_j){
  int i, j;
  int vizinhos=0;
  for (i=(posicao_i-1); i<=(posicao_i+1); i++){
    for (j=(posicao_j-1); j<=(posicao_j+1); j++){
      if (j>=0 && j<42 && i>=0 && i<32 && (i != posicao_i || j!=posicao_j)){
        if (getCelula(i, j) == getCelulaViva()){
          vizinhos++;
        }
      }
    }
  }
  return vizinhos;
}

void Tabuleiro::aplicarRegras(){
  int posicao_i, posicao_j;
  char matriz_provisoria[32][42], cel_matriz;

  for (posicao_i=0; posicao_i<32; posicao_i++){
    for (posicao_j=0; posicao_j<42; posicao_j++){
      if (getCelula(posicao_i, posicao_j) == getCelulaViva() && numVizinhosVivos(posicao_i, posicao_j) < 2){
        matriz_provisoria[posicao_i][posicao_j] = getCelulaMorta();
      }
      else if (getCelula(posicao_i, posicao_j) == getCelulaViva() && numVizinhosVivos(posicao_i, posicao_j) > 3){
        matriz_provisoria[posicao_i][posicao_j] = getCelulaMorta();
      }
      else if (getCelula(posicao_i, posicao_j) == getCelulaMorta() && numVizinhosVivos(posicao_i, posicao_j) == 3){
        matriz_provisoria[posicao_i][posicao_j] = getCelulaViva();
      }
      else {
        matriz_provisoria[posicao_i][posicao_j] = getCelula(posicao_i, posicao_j);
      }
    }
  }
  for (posicao_i=0; posicao_i<32; posicao_i++){
    for (posicao_j=0; posicao_j<42; posicao_j++){
      cel_matriz = matriz_provisoria[posicao_i][posicao_j];
      setCelula(cel_matriz, posicao_i, posicao_j);
    }
  }
}
