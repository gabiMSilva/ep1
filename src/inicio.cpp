#include "inicio.hpp"
#include <iostream>
#include <cstdlib>

using namespace std;

Inicio::Inicio(){
  setResposta(1);
  setGeracoes(0);
  setCelulaViva('O');
  setCelulaMorta(' ');
}
Inicio::~Inicio(){
}
void Inicio::setResposta(int resposta){
  this->resposta = resposta;
}
int Inicio::getResposta(){
  return resposta;
}
void Inicio::setGeracoes(int geracoes){
  this->geracoes = geracoes;
}
int Inicio::getGeracoes(){
  return geracoes;
}
char Inicio::getCelulaViva(){
  return celula_viva;
}
void Inicio::setCelulaViva(char celula_viva){
  this->celula_viva = celula_viva;
}
char Inicio::getCelulaMorta(){
  return celula_morta;
}
void Inicio::setCelulaMorta(char celula_morta){
  this->celula_morta = celula_morta;
}

void Inicio::menu(){
  system("clear");
  cout<< "\n";
  cout<< "***************************************************************************************\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                   GOSPER GLIDER GUN                                 *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                1. JOGAR (PADRÃO)                                    *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                2. FAZER AJUSTES                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                0. SAIR                                              *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "***************************************************************************************\n";
  cout<< "Escolha o numero referente a ação que desejas executar:"<<endl;
  cin>> resposta;

  setResposta(resposta);
}

void Inicio::configurarCharCelulas(){
  char variavel_temporaria;
  system("clear");
  cout<< "\n";
  cout<< "***************************************************************************************\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                   CONFIGURAÇÕES                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                             CARACTER: CÉLULAS VIVAS                                 *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*             DIGITE O CARACTER DESEJADO PARA REPRESENTAR AS CÉLULAS VIVAS            *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "***************************************************************************************\n";
  cin >> variavel_temporaria;
  setCelulaViva(variavel_temporaria);

  system("clear");
  cout<< "***************************************************************************************\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                   CONFIGURAÇÕES                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                             CARACTER: CÉLULAS MORTAS                                *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*             DIGITE O CARACTER DESEJADO PARA REPRESENTAR AS CÉLULAS MORTAS           *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "*                                                                                     *\n";
  cout<< "***************************************************************************************\n";
  cin>> variavel_temporaria;
  setCelulaMorta(variavel_temporaria);
}

  void Inicio::inserirGeracoes(){
    system("clear");
    int variavel_temporaria;
    cout<< "***************************************************************************************\n";
    cout<< "*                                                                                     *\n";
    cout<< "*                                                                                     *\n";
    cout<< "*                                   CONFIGURAÇÕES                                     *\n";
    cout<< "*                                                                                     *\n";
    cout<< "*                                                                                     *\n";
    cout<< "*                                                                                     *\n";
    cout<< "*                                     GERAÇÕES                                        *\n";
    cout<< "*                                                                                     *\n";
    cout<< "*                                                                                     *\n";
    cout<< "*                                                                                     *\n";
    cout<< "*                   DIGITE O NÚMERO INTEIRO DESEJADO DE GERAÇÕES:                     *\n";
    cout<< "*                                                                                     *\n";
    cout<< "*                                                                                     *\n";
    cout<< "*                                                                                     *\n";
    cout<< "*                                                                                     *\n";
    cout<< "*                                                                                     *\n";
    cout<< "*                                                                                     *\n";
    cout<< "*                                                                                     *\n";
    cout<< "*                                                                                     *\n";
    cout<< "*                                                                                     *\n";
    cout<< "*                                                                                     *\n";
    cout<< "***************************************************************************************\n";
    cin>> variavel_temporaria;
    setGeracoes(variavel_temporaria);
  }
