#include <iostream>
#include <time.h>
#include "block.hpp"
#include "blinker.hpp"
#include "glider.hpp"
#include "tabuleiro.hpp"
#include "inicio.hpp"


int main(){
  int posicao_i, posicao_j, contador;
  char resposta;
  char variavel_temporaria;
  char universo[32][42];
  clock_t start;
  Tabuleiro jogo;
  Block block;
  Blinker blinker;
  Glider glider;
  Inicio menu;

  while(1){
    menu.menu();
      if (menu.getResposta() == 1){
      menu.setGeracoes(200);
      menu.setCelulaMorta(' ');
      menu.setCelulaViva('O');
      break;
      }
      else if (menu.getResposta() == 2){
        system("clear");
        menu.inserirGeracoes();
        system("clear");
        menu.configurarCharCelulas();
        break;
      }
      else if (menu.getResposta()==0){
        cout << "Você deseja mesmo sair?(s/n)";
        cin >> resposta;
        if (resposta == 'n' || resposta == 'N'){
          system("clear");
        }
        else if (resposta == 'S' || resposta == 's'){
          return 0;
        }
      }
      else {
        system("clear");
        cout << "Digite um valor válido";
      }
    }

    blinker.setCelulaMorta(menu.getCelulaMorta());
    block.setCelulaMorta(menu.getCelulaMorta());
    glider.setCelulaMorta(menu.getCelulaMorta());
    jogo.setCelulaMorta(menu.getCelulaMorta());

    blinker.setCelulaViva(menu.getCelulaViva());
    block.setCelulaViva(menu.getCelulaViva());
    glider.setCelulaViva(menu.getCelulaViva());
    jogo.setCelulaViva(menu.getCelulaViva());

    jogo.criarForma();
    block.criarForma();
    blinker.criarForma();
    glider.criarForma();

    for (posicao_i = 0; posicao_i<32; posicao_i++){
      for (posicao_j=0; posicao_j<42; posicao_j++){
        if (jogo.getCelula(posicao_i, posicao_j) == menu.getCelulaViva()){
          universo[posicao_i][posicao_j] = jogo.getCelula(posicao_i, posicao_j);
        }
       else if (block.getCelula(posicao_i, posicao_j) == menu.getCelulaViva()){
         universo[posicao_i][posicao_j] = block.getCelula(posicao_i, posicao_j);
        }
        else if (blinker.getCelula(posicao_i, posicao_j) == menu.getCelulaViva()){
          universo[posicao_i][posicao_j] = blinker.getCelula(posicao_i, posicao_j);
        }
        else if (glider.getCelula(posicao_i, posicao_j) == menu.getCelulaViva()){
          universo[posicao_i][posicao_j] = glider.getCelula(posicao_i, posicao_j);
        }
        else {
          universo[posicao_i][posicao_j] = menu.getCelulaMorta();
        }
      }
    }
    for (posicao_i= 0; posicao_i<32; posicao_i++){
      for (posicao_j=0; posicao_j<42; posicao_j++){
        variavel_temporaria = universo[posicao_i][posicao_j];
         jogo.importarMatriz(variavel_temporaria, posicao_i, posicao_j);
      }
    }
    jogo.imprimirMatriz();
    for(contador=0; contador< menu.getGeracoes(); contador++){
      system("clear");
      jogo.aplicarRegras();
      jogo.imprimirMatriz();
      start = clock();
      while ((clock()-start)<=100000){
        if ((clock()-start == 100000)){
         system("clear");
          break;
      }
    }
  }
  return 0;
}
