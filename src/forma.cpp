#include "forma.hpp"
#include <iostream>
#include <stdio.h>

Forma::Forma(){
  setCelulaViva('O');
  setCelulaMorta(' ');
  setCelula(' ', 0, 0);
}
Forma::~Forma(){
}
char Forma::getCelulaViva(){
  return celula_viva;
}
void Forma::setCelulaViva(char celula_viva){
  this->celula_viva = celula_viva;
}
char Forma::getCelulaMorta(){
  return celula_morta;
}
void Forma::setCelulaMorta(char celula_morta){
  this->celula_morta = celula_morta;
}
char Forma::getCelula(int posicao_i, int posicao_j){
  char variavel_temporaria;
  variavel_temporaria = tabuleiro[posicao_i][posicao_j];
  return variavel_temporaria;
}
void Forma::setCelula(char celula, int posicao_i, int posicao_j){
  tabuleiro[posicao_i][posicao_j] = celula;
}
void criarForma(){
}
